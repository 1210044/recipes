from contextlib import asynccontextmanager
from typing import List

from fastapi import FastAPI, HTTPException
from sqlalchemy import desc
from sqlalchemy.future import select

import models
import schemas
from database import engine, session


@asynccontextmanager
async def lifespan(app: FastAPI):
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)
    yield
    await session.close()
    await engine.dispose()


app = FastAPI(lifespan=lifespan)


@app.get("/recipes", response_model=List[models.Recipe])
async def get_recipes() -> List[schemas.RecipeShort]:
    result = await session.execute(
        select(models.Recipe).order_by(
            desc(models.Recipe.views), models.Recipe.cooking_time
        )
    )
    return result.scalars().all()


@app.get(
    "/recipes/{idx}",
    responses={
        200: {"model": schemas.RecipeDetail},
        404: {
            "model": schemas.ErrorResponse,
            "description": "Recipe Not Found",
        },
    },
)
async def get_recipe_by_idx(idx: int) -> models.Recipe:
    result = await session.execute(
        select(models.Recipe).where(models.Recipe.id == idx)
    )
    recipe = result.scalars().first()
    if recipe is None:
        raise HTTPException(status_code=404, detail="Recipe not found")

    recipe.views += 1
    await session.commit()
    return recipe
