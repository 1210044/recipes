from typing import List

from pydantic import BaseModel, ConfigDict


class BaseRecipe(BaseModel):
    title: str
    cooking_time: int


class RecipeIn(BaseRecipe):
    description: str


class RecipeShort(BaseRecipe):
    views: int
    model_config = ConfigDict(from_attributes=True)


class RecipeDetail(RecipeShort, RecipeIn):
    ingredients: List["Ingredient"] = []


class Ingredient(BaseModel):
    title: str


class ErrorResponse(BaseModel):
    detail: str
