from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import relationship

from database import Base


class Recipe(Base):
    __tablename__ = "recipes"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    cooking_time = Column(Integer, index=True)
    views = Column(Integer, index=True, default=0)
    description = Column(String)
    recipes_ingredients = relationship(
        "RecipeIngredient",
        back_populates="recipe",
        cascade="all, delete-orphan",
        lazy="selectin",
    )
    ingredients = association_proxy(
        "recipes_ingredients",
        "ingredient",
        creator=lambda ingredient: RecipeIngredient(ingredient=ingredient),
    )


class Ingredient(Base):
    __tablename__ = "ingredients"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    recipes_ingredients = relationship(
        "RecipeIngredient",
        back_populates="ingredient",
        cascade="all, delete-orphan",
        lazy="selectin",
    )
    recipes = association_proxy(
        "recipes_ingredients",
        "recipe",
        creator=lambda recipe: RecipeIngredient(recipe=recipe),
    )


class RecipeIngredient(Base):
    __tablename__ = "recipes_ingredients"
    id = Column(Integer, primary_key=True)
    recipe_id: Column = Column(ForeignKey("recipes.id"), nullable=False)
    ingredient_id: Column = Column(
        ForeignKey("ingredients.id"), nullable=False
    )
    recipe = relationship(
        "Recipe", back_populates="recipes_ingredients", lazy="selectin"
    )
    ingredient = relationship(
        "Ingredient",
        back_populates="recipes_ingredients",
        lazy="selectin",
    )
